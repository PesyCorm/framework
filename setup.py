from setuptools import setup, find_packages


setup(
    name='simple_test',
    version='1.0',
    description='Фреймворк для написания автотестов',
    url='https://gitlab.com/PesyCorm/framework',
    install_requires=['selenium', 'Pillow', 'pytest', 'allure-pytest', 'requests'],
    packages=['simple_test'],
    include_package_data=True
)