from selenium.webdriver.common.by import By
from selenium import webdriver

from .element import Element


__all__ = ('Page', 'set_parent', 'page_name')


class Page:

    driver = None
    how = None
    locator = None
    name = None

    @classmethod
    def _set_obj(cls, browser):
        cls.browser = browser
        cls.driver = cls.browser.driver

    def __init__(self):
        self.__install_parent_for_elements()

    def __install_parent_for_elements(self):
        """Устанавливаем локатор пейджа как родительский локатор для элементов в пейдже"""

        if self.how and self.locator:
            for attr in self.__dir__():
                attr = getattr(self, attr)    # Некрасиво
                if isinstance(attr, Element) and attr._get_parent:
                    attr.page_how = self.how
                    attr.page_locator = self.locator


def set_parent(how, locator=None):
    """Декоратор для Page
    Указываем в нем родительский элемент, все остальные элементы будут искаться от родительского
    """

    if locator is None:
        locator = how
        how = By.CSS_SELECTOR

    def class_decorator(clazz):
        clazz.how = how
        clazz.locator = locator
        return clazz

    return class_decorator

def page_name(name):
    """Название страницы"""

    def class_decorator(clazz):
        clazz.name = 'Страница ' + name   # ?
        return clazz

    return class_decorator