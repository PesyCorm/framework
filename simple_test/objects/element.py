import time
from typing import Union

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.common.exceptions import WebDriverException

from ..core.logger import info
from ..core.config import Config

from ..common.waitings import *
from ..common.waitings import wait_func
from ..common.waitings import calculate_waiting_time


__all__ = ('Element',)


class Element:
    """Базовый класс для элементов"""

    driver = None
    page_how = None
    page_locator = None
    page_name = None

    @classmethod
    def _set_obj(cls, driver):
        cls.driver = driver

    def __init__(self, how, locator, name, page_parent=True):
        """Базовый класс всех элементов
        :param how: как ищем
        :param locator: локатор
        :param name: название элемента (для логирования)
        :param page_parent: наследовать родителя от пейджа
        """

        self.how = how
        self.locator = locator
        self.name = str(name)
        self._get_parent = page_parent

        self.config = Config()
        self.wait_element_load = self.config.get('WAIT_ELEMENT_LOAD')  # Не очень

    def _webelement(self, return_list = False, wait_time=False):
        """Вычисляем webelement по заданному локатору и стратегии поиска

        Метод пытается найти webelement self.wait_element_load секунд.
        Если webelement не найден за указанное время, возвращает False
        :param return_list: возвращать список webelement или 1 webelement
        :param wait_time: True - взять из конфига, False - 0 сек. или передать свой вариант
        """
        page_element = None
        wait_time = calculate_waiting_time(wait_time)   # а надо ли это вообще?

        if self.page_how and self.page_locator:
            page_element = self._find_element(driver=self.driver, how=self.page_how, locator=self.page_locator,
                                              wait_time=wait_time)
            assert page_element, f'Не удалось обнаружить страницу {self.page_name} ' \
                                 f'с локатором:\n{str(self.page_how)}: {self.page_locator}\n'

        elements = self._find_element(driver=(page_element[0] if page_element else self.driver), how=self.how,
                                      locator=self.locator, wait_time=wait_time)

        if not elements:
            return False
        return elements if return_list else elements[0]

    def _find_element(self, driver: Union[WebDriver, WebElement], how: By, locator: str,
                      wait_time: Union[bool, int] = False):
        end_time = time.time() + wait_time
        while True:
            try:
                return driver.find_elements(how, locator)
            except WebDriverException:
                pass
            if end_time >= time.time():
                return False
            time.sleep(0.1)

    def highlight(self):
        """Подсветка элемента (для дебага)"""
        self.execute_script("arguments[0].style.border = '5px solid red'")
        return self

    def _highlight_action(self):
        """Подсветка элемента"""
        self.execute_script("function setProperty(element, color) {element.style.outline = color};"
                "setProperty(arguments[0], '2px solid red');"
                "setTimeout(setProperty, 400, arguments[0], '')")
        return self

    def _get_locator(self) -> list:
        """Получаем локатор элемента (вместе с родителем, если есть)"""
        result = []
        if self.page_how and self.page_locator:
            result.append(f'\n{str(self.page_how)}: {self.page_locator}')
        result.append(f'{str(self.how)}: {self.locator}\n')
        return result

    def print_locator(self):
        """Выводим локатор элемента"""
        for locator in self._get_locator():
            print(locator)

    @property
    def is_displayed(self) -> bool:
        """Воозвращает True, если webelement отображается для пользователя"""
        return bool(self._webelement())

    @property
    def text(self):
        """Возвращает текст, который содержится в элементе"""
        return self._webelement().text

    @property
    def count_elements(self):
        """Возвращает кол-во элементов на странице"""
        return len(self._webelement(return_list=True))

    def click(self) -> 'Element':
        """Клик ЛКМ по элементу"""
        self.wait(Displayed)
        self._webelement().click()
        info(f'Кликнули на элемент {self.name}')
        return self

    def clear(self) -> 'Element':
        """Очищаем поле ввода"""
        self.wait(Displayed)
        self._webelement().clear()
        info(f'Очистили текст в элементе {self.name}')
        return self

    def type_in(self, text) -> 'Element':
        """Вводим текст в элемент
        :param text: текст который вводим
        """
        self.wait(Displayed)
        self._webelement().send_keys(text)
        info(f'Ввели "{text}" в элемент {self.name}')
        return self

    def execute_script(self, script):
        """Вставляем скрипт в элемент
        :param script: скрипт
        """
        self.driver.execute_script(script, self.wait(Displayed)._webelement())
        info(f'Вставили в элемент {self.name} скрипт:\n{script}')
        return self

    def wait(self, *conditions, wait_time=True, msg=''):
        """Ожидаем состояние элемента
        :param conditions: состояния (из waitings.py)
        :param wait_time: True - взять из конфига, False - 0 сек. или передать свой вариант
        :param msg: сообщение в лог, в случае если не дождемся
        """
        wait_func(*conditions, element=self, wait_time=wait_time, err_msg=msg, expected_result=True)
        return self

    def wait_not(self, *conditions, wait_time=True, msg=''):
        """Ожидаем, что элемент не будет находится в переданном состоянии
        :param conditions: состояния (из waitings.py)
        :param wait_time: True - взять из конфига, False - 0 сек. или передать свой вариант
        :param msg: сообщение в лог, в случае если не дождемся
        """
        wait_func(*conditions, element=self, wait_time=wait_time, err_msg=msg, expected_result=False)
        return self