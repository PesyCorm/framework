import time
from typing import Callable

from selenium import webdriver

from ..core.logger import _system_log
from ..core.config import Config

from ..common.assert_that import *
from ..common.waitings import wait_func


__all__ = ('Browser',)


class Browser:

    browser_settings = {
        'BROWSER': '',
        'BROWSER_VERSION': '',
        'BROWSER_RESOLUTION': '',
        'HEADLESS_MODE': False,
    }
    name = 'Браузер'

    def __init__(self):
        self.driver = None
        self.config = Config()
        self._parse_browser_settings(self.config)
        self._run_browser()

        self.wait_element_load = self.config.get('WAIT_ELEMENT_LOAD') # Не очень

    def _parse_browser_settings(self, config):
        """Получаем настройки браузера из конфига"""
        for setting in self.browser_settings.keys():
            self.browser_settings[setting] = config.get(setting)

    def _run_browser(self):
        """Запускаем браузер"""
        browser_to_run = self.browser_settings.get('BROWSER')
        assert browser_to_run, 'Не указан браузер'
        browser_to_run = browser_to_run.lower()
        if browser_to_run == 'chrome':
            if self.config.get('SELENOID'):
                self._run_chrome_docker()
            else:
                self._run_chrome_local()
        else:
            # пока что достаточно chrome
            raise AssertionError(f'Требуемый браузер {browser_to_run} пока что не поддерживается для запуска')
        _system_log(f'Запустили браузер {browser_to_run.upper()}')

    def _run_chrome_local(self):
        """Запускаем в Chrome локально (не в докер)"""
        options = webdriver.ChromeOptions()
        bs = self.browser_settings

        options.add_argument(f'--window-size={bs.get("BROWSER_RESOLUTION")}')
        options.add_argument('--ignore-certificate-errors-spki-list')
        options.add_argument('--ignore-certificate-errors')
        options.add_argument('--ignore-ssl-errors')
        if bs.get('HEADLESS_MODE'):
            options.add_argument('--headless')
        self.driver = webdriver.Chrome(chrome_options=options)

    def _run_chrome_docker(self):
        """Запуск Chrome в докере (selenoid)"""
        capabilities = {
            "browserName": "chrome",
            "browserVersion": '102.0', #!!!
            "selenoid:options": {
                "enableVNC": self.config.get('SELENOID_VNC_ENABLE'),
                "enableVideo": self.config.get('SELENOID_VIDEO_ENABLE')
            }
        }

        self.driver = webdriver.Remote(
            command_executor="http://localhost:4444/wd/hub",
            desired_capabilities=capabilities)

    @property
    def current_url(self):
        """Получаем url текущего окна."""
        return self.driver.current_url

    def open(self, url: str):
        """Метод для открытия страницы
        :param url: url
        """
        if not url.startswith('https://') or not url.startswith('http://'):
            url = 'https://' + url
        self.driver.get(url)
        _system_log(f'Перешли по адресу {url}')

    @property
    def current_title(self):
        """Получаем заголовок текущего окна."""
        return self.driver.title

    @property
    def count_windows(self):
        """Получаем кол-во открытых окон"""
        return len(self.driver.window_handles)

    def page_source(self):
        """Получаем источник (исходный код) текущей страницы"""
        return self.driver.page_source

    def check_page_change(self, action: Callable):
        """Проверяем изменения на странице после действия
        :param action: действия
        """
        current_html = self.page_source()
        action()
        assert_that(self.page_source, NotEqual(current_html), desc='Страница не изменилась', wait_time=True)

    def switch_to_opened_window(self):
        """Переключается на последнее открытое окно"""
        if self.count_windows >= 1:
            self.driver.switch_to.window(self.driver.window_handles[-1])
        _system_log('Переключились на последнюю открытую вкладку')

    def quit(self):
        """Закрывает браузер"""
        self.driver.quit()
        _system_log('Завершили сессию браузера')

    def execute_script(self, script):
        """Выполнить js скрипт
        :param script: скрипт
        """
        self.driver.execute_script(script)
        _system_log(f'Вставили скрипт\n{script}')

    def wait(self, *conditions, wait_time=True, msg=''):
        """Ожидаем состояние браузера
        :param conditions: состояния (из waitings.py)
        :param wait_time: True - взять из конфига, False - 0 сек. или передать свой вариант
        :param msg: сообщение в лог, в случае если не дождемся
        """
        wait_func(*conditions, element=self, wait_time=wait_time, err_msg=msg, expected_result=True)
        return self

    def wait_not(self, *conditions, wait_time=True, msg=''):
        """Ожидаем, что браузер не будет находится в переданном состоянии
        :param conditions: состояния (из waitings.py)
        :param wait_time: True - взять из конфига, False - 0 сек. или передать свой вариант
        :param msg: сообщение в лог, в случае если не дождемся
        """
        wait_func(*conditions, element=self, wait_time=wait_time, err_msg=msg, expected_result=False)
        return self

    def save_screen(self):
        """Делаем скриншот окна и сохраняем в папке report/screens
        :return: название файла скриншота
        """
        screen_name = 'auto-screen-' + str(time.time()) + '.png'
        self.driver.save_screenshot(f'report/screens/{screen_name}')
        _system_log(f'Сохранили скриншот {screen_name}')
        return screen_name
