from typing import Union

from ..core.logger import info

from .element import Element

from ..common.waitings import *
from ..common.waitings import wait_func


__all__ = ('ElementsList',)


class Item:
    """Элемент из списка элементов"""

    def __init__(self, list_example, number=None, contains_text=None, wait_element_load=True):
        self.list_example = list_example
        self.number = number
        self.contains_text = contains_text
        self.wait_element_load = wait_element_load
        self.name = list_example.name

    def _item(self, wait_time=False):
        """Вычисляем webelement по заданному локатору и стратегии поиска

        Метод пытается найти webelement self.wait_element_load секунд.
        Если webelement не найден за указанное время, возвращает False
        :param wait_time: True - взять из конфига, False - 0 сек. или передать свой вариант
        """
        items = self.list_example._webelement(return_list=True, wait_time=wait_time)
        if items:
            # Работает ок, но если выполняем действие с элементом без ожидания, а элемента нет, то трейсбек нелогичный получается
            if self.number is not None:
                return self._find_item_by_number(items)
            elif self.contains_text:
                return self._find_item_by_text(items)
            else:
                raise AssertionError('Не переданы обязательные параметры для Item')

    def _find_item_by_number(self, items):
        """Ищем элемент в полученном списке элементов по номеру
        :param items: список элементов
        """
        if len(items) >= self.number:
            result =  items[self.number]
        else:
            result =  False
        return result

    def _find_item_by_text(self, items):
        """Ищем элемент в полученном списке элементов по тексту
        :param items: список элементов
        """
        try:
            for item in items:
                if self.contains_text in item.text:
                    return item
        except:
            return False

    def highlight(self):
        """Подсветка элемента
        ..Нужно еще один метод сделать для записи видео с этим скриптом
        "function setProperty(element, color) {element.style.outline = color};"
                "setProperty(arguments[0], '2px solid red');"
                "setTimeout(setProperty, 400, arguments[0], '')"
        """
        self.driver.execute_script("arguments[0].style.border = '5px solid red'", self._item())
        return self

    def _get_locator(self) -> list:
        """Получаем локатор элемента (вместе с родителем, если есть)"""
        result = []
        result += self.list_example._get_locator()
        if self.number:
            result.append(f'С порядковым номером: {self.number}\n')
        else:
            result.append(f'Содержащий текст: {self.contains_text}\n')
        return result

    def print_locator(self):
        """Выводим локатор элемента"""
        for locator in self._get_locator():
            print(locator)

    @property
    def is_displayed(self) -> bool:
        """Воозвращает True, если webelement отображается для пользователя"""
        return bool(self._item())

    @property
    def text(self):
        """Возвращает текст, который содержится в элементе"""
        return self._item().text

    def click(self):
        """Делаемклик по итему"""
        #self.wait(Displayed)
        self._item().click()
        info(f'Кликнули по элементу из списка {self.list_example.name}')
        return self

    def wait(self, *conditions, wait_time=True, msg=''):
        """Ожидаем состояние элемента
        :param conditions: состояния (из waitings.py)
        :param wait_time: True - взять из конфига, False - 0 сек. или передать свой вариант
        :param msg: сообщение в лог, в случае если не дождемся
        """
        wait_func(*conditions, element=self, wait_time=wait_time, err_msg=msg, expected_result=True)
        return self

    def wait_not(self, *conditions, wait_time=True, msg=''):
        """Ожидаем, что элемент не будет находится в переданном состоянии
        :param conditions: состояния (из waitings.py)
        :param wait_time: True - взять из конфига, False - 0 сек. или передать свой вариант
        :param msg: сообщение в лог, в случае если не дождемся
        """
        wait_func(*conditions, element=self, wait_time=wait_time, err_msg=msg, expected_result=False)
        return self


class ElementsList(Element):
    """Список элементов"""

    def item(self, number=None, contains_text=None):
        """Получаем элемент из списка элементов
        :param number: порядковый номер элемента
        :param contains_text: вхождение текста в элемент
        :param wait_time: True - взять из конфига, False - 0 сек. или передать свой вариант
        """
        return Item(self, number, contains_text, wait_element_load=self.wait_element_load)