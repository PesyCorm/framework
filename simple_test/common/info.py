import platform


__all__ = ('host_os', 'encoding', 'calculate_waiting_time')


host_os = platform.platform()
encoding = 'Windows-1251' if host_os.lower().startswith('win') else 'utf-8'