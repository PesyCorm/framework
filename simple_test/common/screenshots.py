from PIL import Image, ImageChops
from time import time

from ..core.logger import _system_log


__all__ = ('compare_screens',)


def compare_screens(screen1, screen2, equal=True):  # Не точно работает под виндой
    """Сравниваем два изображения
    :param screen1: первое изображение
    :param screen2: второе изображение
    :param equal: изображения должны быть равны
    """
    obj1 = Image.open(f'report/screens/{screen1}')
    obj2 = Image.open(f'report/screens/{screen2}')
    _system_log(f'Проверяем, что изображения {screen1} и {screen2} {"равны" if equal else "различаются"}')
    diff = ImageChops.difference(obj1, obj2)
    result = diff.getbbox()
    if (result is None and equal) or (result is not None and not equal):
        return
    elif (result is None and not equal):
        raise AssertionError(f'Изображения {screen1} и {screen2} не должны быть равны')
    else:
        difference_image = f'difference{time()}.png'
        diff.save(difference_image)
        raise AssertionError(f'Изображения {screen1} и {screen2} не равны. '
                             f'Разница показана на изображении report/screens/{difference_image}')