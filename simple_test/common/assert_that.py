from typing import Any
import time

from .waitings import calculate_waiting_time


__all__ = ('assert_that', 'EqualTo', 'NotEqual')


class EqualTo:

    def __init__(self, item2: Any):
        self.item2 = item2

    def match(self, item1: Any) -> list:
        result = item1 == self.item2
        return result


class NotEqual:

    def __init__(self, item2: Any):
        self.item2 = item2

    def match(self, item1: Any) -> list:
        result = item1 != self.item2
        return result


def assert_that(arg1: Any, arg2: Any, desc: str, wait_time: Any = False) -> None:
    """Комплексные ассерты
    :param arg1: первый аргумент сравнения (что сравниваем)
    :param arg2: второй аргумент сравнения (с чем сравниваем)
    :param desc: текстовое описание ошибки
    :param wait_time: True - взять из конфига, False - 0 сек. или передать свой вариант

    """
    wait_time = calculate_waiting_time(wait_time)

    while wait_time >= 0:
        error = None
        try:
            item = arg1
            if callable(item):
                item = item()

            rslt = arg2.match(item)
            if isinstance(rslt, bool):
                if not rslt:
                    raise AssertionError(desc + "\n")
            break

        except Exception as err:
            error = err

        if wait_time:
            time.sleep(0.1)
            wait_time -= 0.1

    if error:
        raise error


