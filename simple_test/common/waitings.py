import time

from ..core.config import Config


__all__ = ('Displayed', 'ContainsText', 'CountElement', 'ExactText', 'CountWindows', 'UrlContains')


_wait_element_load = Config().get('WAIT_ELEMENT_LOAD')


def calculate_waiting_time(wait_time=True):
    """Считаем время ожидания
    :param wait_time: True - взять из конфига, False - 0 сек. или передать свой вариант
    """
    if type(wait_time) is not int:
        wait_time =  _wait_element_load if wait_time else 0
    return wait_time


class BaseCondition:
    """Базовый класс для всех состояний
    Нужен для проверки isinstance
    """


class Displayed(BaseCondition):

    template = 'отображаться'
    # мб стоит убрать в инит? единственный класс который нельзя проверить на инстанс т.к. передаем неинициализрованным

    @staticmethod
    def match(element):
        return element.is_displayed


class ContainsText(BaseCondition):

    def __init__(self, text):
        if not text:
            raise ValueError('Нет смысла проверять вхождение пустого текста в строку!')
        self.text = text
        self.template = f'содержать текст "{self.text}"'

    def match(self, element):
        return self.text in element.text


class ExactText(BaseCondition):

    def __init__(self, text):
        if not text:
            raise ValueError('Нет смысла проверять вхождение пустого текста в строку!')
        self.text = text
        self.template = f'содержать текст "{self.text}"'

    def match(self, element):
        return self.text == element.text


class CountElement(BaseCondition):

    def __init__(self, count):
        if type(count) != int:
            raise TypeError('Неверный тип, должен быть int')
        self.count = count
        self.template = f'иметь кол-во элементов {self.count}'

    def match(self, element):
        return self.count == element.count_elements


class CountWindows(BaseCondition):
    """Проверяет количество открытых окон браузера"""

    def __init__(self, count):
        self.count = count
        self.template = f'иметь кол-во открытых окон {self.count}'

    def match(self, driver):
        if not hasattr(driver, 'count_windows'):
            raise TypeError('Состояние предназначено для класса Browser')
        return self.count == driver.count_windows


class UrlContains(BaseCondition):
    """Проверяем что url содержит эталонный текст"""

    def __init__(self, url):
        self.url = url
        self.template = f'содержать url {self.url}'

    def match(self, browser):
        if not hasattr(browser, 'current_url'):
            raise TypeError('Состояние предназначено для класса Browser')
        return self.url in browser.current_url


def wait_func(*conditions: BaseCondition, element, wait_time: int, err_msg: str, expected_result: bool):

    assert conditions, 'Необходимо передать хотя бы 1 состояние!'
    wait_time = calculate_waiting_time(wait_time)

    for condition in conditions:
        # if not isinstance(condition, BaseCondition):
        #     raise TypeError('Вы передали в wait НЕ состояние!')
        end_time = time.time() + wait_time
        err_temlate = element.name + (' должен ' if expected_result else ' не должен ') + condition.template
        while True:
            result = condition.match(element)
            if expected_result and result:
                return
            elif not expected_result and not result:
                return
            if time.time() >= end_time:
                raise AssertionError(f'Не дождались состояния элемента {element.name}\n'+(err_msg if err_msg else err_temlate))