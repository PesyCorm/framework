import os
import sys
import subprocess

from .config import Config

from ..common.info import encoding


__all__ = ('RunTests',)


class RunTests:

    test_id = 1     #!
    test_files = []
    processes = []
    result_template = f'\n\n{"-"*70}\n' + \
                      ' '*10 + 'Тестовый набор {} прошел {}' + \
                      '\n\n{}' + f'\n{"-"*70}\n\n'

    def __init__(self):
        self.args_str = ' '.join(sys.argv[1:])   # Не очень
        config = Config()
        self.streams_number = config.get('STREAMS_NUMBER')
        self.tests_dir = config.get('TESTS_DIR')

    def run_tests(self):
        self._collect_test_files()
        self._process_manager()

    def _collect_test_files(self):
        self.test_files = [test for test in os.listdir(self.tests_dir) if test.startswith('test_')]

    def _process_manager(self):
        while self.test_files or len(self.processes) > 0:
            if len(self.processes) < self.streams_number and self.test_files:
                run_test = self.test_files.pop()
                self._run_test(run_test)
                self.test_id += 1
            else:
                for proc_i, proc in enumerate(self.processes):
                    if proc[0].poll() is not None:
                        self._print_result(*self.processes.pop(proc_i))
                        break

    def _run_test(self, test_name):
        test_cmd = f'{sys.executable} {os.path.join(self.tests_dir, test_name)} ' \
                   f'{self.args_str} --TEST_ID {self.test_id}'

        print('Запускаем тесты: ', test_cmd)
        test_process = subprocess.Popen(test_cmd.split(),
                                        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.processes.append((test_process, test_name))

    def _print_result(self, test_process, test_name):
        result_status = 'успешно' if test_process.poll() == 0 else 'с ошибкой'  # Не работает
        result_output = test_process.communicate()
        try:
            result_info = result_output[0].decode(encoding) + result_output[1].decode(encoding)
            result = self.result_template.format(test_name, result_status, result_info)
            print(result)
        except Exception as err:
            print(f'При попытке вывести результаты тестов {test_name} возникла ошибка\n: {err}')