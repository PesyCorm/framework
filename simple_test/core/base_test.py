import os
import shutil
import sys # Убрать!
sys.path.append(os.path.join(sys.path[0], '..'))

import requests
import pytest

from .config import Config

from ..objects.browser import Browser
from ..objects.page import Page
from ..objects.element import Element


__all__ = ('BaseTest',)


class BaseTest:

    @classmethod
    @pytest.fixture(scope='class', autouse=True)
    def _set_up_class(cls, request):
        request.addfinalizer(cls._teardown_class)
        cls._run_environment()
        cls._check_site_availability()
        cls._pass_obj()
        if cls.config.get('TEST_ID') == 1:
            cls._clear_report_folder()

    @classmethod
    def _teardown_class(cls):
        try:
            cls.browser.quit()
        except:
            pass

    @classmethod
    def _run_environment(cls):
        cls.config = Config()
        cls.browser = Browser()
        cls.driver = cls.browser.driver

    @classmethod
    def _check_site_availability(cls):
        """Проверяем доступность сайтов указанных в конфиге"""
        sites = cls.config.get('SITES')
        if sites:
            for site in sites.split(','):
                site = site.strip()  # На случай если в конфиге указали лишний пробел перед запятой
                if not site.startswith('https://') or not site.startswith('http://'):
                    site = 'https://' + site
                if not requests.get(site):
                    raise RuntimeError(f'Недоступен сайт {site}')

    @classmethod
    def _pass_obj(cls):
        Page._set_obj(cls.browser)
        Element._set_obj(cls.driver)

    @staticmethod
    def _clear_report_folder():
        """Пересоздаем папку с отчетами"""
        folders = ('screens', 'logs')  # Папка для логов все еще нужна?
        shutil.rmtree('report', ignore_errors=True)
        os.mkdir('report')
        for folder in folders:
            os.mkdir(os.path.join('.', 'report', folder))