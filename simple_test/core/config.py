import os
import sys
import ast
import argparse
import configparser

from typing import Any


__all__ = ('Config',)


# Настройки, которые указываем в файле конфига
ConfigSettings = dict(
    SITES="",  # список сайтов через запятую, которые надо проверить перед запуском тестов
    LOGGER_LVL='INFO',  # Уровень вывода логов
    BROWSER='chrome',  # Браузер
    WAIT_ELEMENT_LOAD=20,  # время ожидания загрузки элемента
    BROWSER_RESOLUTION='1920,1080',  # разрешение браузера
)

# Настройки, которые передаем из командной строки
CommandlineSettings = dict(
    STREAMS_NUMBER = {'default': 1, 'type': int, 'help': 'Число одновременно запущенных наборов тестов'},
    SELENOID = {'default': False, 'type': bool, 'help': 'Запускаем в selenoid'},
    SELENOID_VNC_ENABLE = {'default': False, 'type': bool, 'help': 'Поддержка vnc в selenoid'},
    SELENOID_VIDEO_ENABLE = {'default': False, 'type': bool, 'help': 'Запись видео в selenoid'},
    HEADLESS_MODE = {'default': False, 'type': bool, 'help': 'Запуск браузера в headless'},
    HIGHLIGHT_ACTION = {'default': False,  'type': bool, 'help': 'Подсветка действий в тесте'},
    TEST_ID = {'default': 1,  'type': int, 'help': 'Идентификатор теста'},
    TESTS_DIR = {'default': '.', 'type': str, 'help': 'Путь до директории с тестами'} #?
)


class Config:
    """Класс для работы с config.ini"""

    intance = None

    other_args = [] #?

    def __new__(cls, *args, **kwargs):
        if cls.intance is None:
            cls.intance = super(Config, cls).__new__(cls)
        return cls.intance

    def __init__(self):
        if not hasattr(self, 'config'):
            self.__config_instance()
            self.__set_args_from_commandline()
            self.__parse_config()
            #self.__collect_config()

    def __config_instance(self):
        self.config = configparser.ConfigParser(default_section='general')
        self.config.read_dict({'general': ConfigSettings})
        self.config.add_section('custom')

    def __parse_config(self):
        config_path = os.path.join('.', self.get('TESTS_DIR'), 'config.ini')
        self.config.read(config_path)

    def __set_args_from_commandline(self):
        """Устанавливаем параметры из командной строки"""
        parser = argparse.ArgumentParser()

        #parser.add_argument('file_name', type=str, nargs='?', default='main')  # !
        for arg, params in CommandlineSettings.items():
            parser.add_argument(f'--{arg}', **params)

        args, self.other_args = parser.parse_known_args()
        #self.set('file_name', args.file_name)
        for arg in CommandlineSettings.keys():
            self.set(arg, vars(args)[arg])

    def _get_other_args(self):
        return self.other_args

    def get(self, option: str, sections: tuple = ('general', 'custom')):
        """Возвращает значение опции
        :param option: опция
        :param sections: в каких секциях ишем
        :return: значение опции если нашли, иначе None
        """
        for section in sections:
            value = self.config.get(section, option, fallback=None)
            return self._convert(value)

    def set(self, option: str, value: Any, section='general'):
        """Устанавливаем опцию со значением в конфиге
        :param option: опция
        :param value: значение
        :param section: секция
        """
        self.config.set(section, str(option.upper()), str(value))

    @staticmethod
    def _convert(value: Any):
        """Пробуем преобразовать значение к соответствующему типу
        :param value: значение
        """
        value = str(value)
        try:
            value = ast.literal_eval(value)
        except:
            pass  # Оставляем строкой, если не получилось преобразовать
        return value