import pytest
from .config import Config
import os


__all__ = ('run_test_file',)


def run_test_file(file_name: str):
    """Запускаем тестовый файл"""
    config = Config()
    pytest_args = config._get_other_args()
    file_path = os.path.join(config.get('TESTS_DIR'), file_name)
    pytest_args.append(file_path)
    pytest.main(pytest_args)