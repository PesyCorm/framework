import logging


__all__ = ('log', 'info', 'warning')


__logger = logging.getLogger()


def __base_log(msg, level='DEBUG'):
    """Запись лога
    :param text: сообщение
    :param level: уровень логирования (DEBUG, INFO, WARNING, ERROR)
    """
    try:
        msg = str(msg)
    except Exception as error:
        print("Не смогли преобразовать переданный в log объект к строке\n{0}".format(error))
        return
    level = logging.getLevelName(level)
    __logger.log(level=level, msg=msg)

def _system_log(msg):
    """Лог компонентов фреймворка"""
    __base_log(msg=f'[s] {msg}', level='INFO')

def log(msg):
    """Лог теста
    :param msg: сообщение
    """
    __base_log(msg=f'  [l] {msg}', level='INFO')

def info(msg):
    """ИНФО
    :param msg: сообщение
    """
    __base_log(msg=f'      [i] {msg}', level='INFO')

def warning(msg):
    """Предупреждение
    :param msg: сообщение
    """
    __base_log(msg=msg, level='WARNING')